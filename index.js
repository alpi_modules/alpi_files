var basis = require('alpi_basis');
var filters = require('alpi_filters');
var fs = require('fs');

module.exports = {
    /**
     * Checks if the specified path exists
     * 
     * @param {String} path 
     * @returns {Boolean}
     */
    PathExists: function (path) {
        return require('fs').existsSync(filters.Other.NormalizePath(path));
    },
    IsDirectory: function (path) {
        return module.exports.PathExists(path) == true ? fs.lstatSync(path).isDirectory() : false;
    },
    IsFile: function (path) {
        return module.exports.PathExists(path) == true ? fs.lstatSync(path).isFile() : false;
    },
    IsSymLink: function (path, callback) {
        return module.exports.PathExists(path) == true ? fs.lstatSync(path).isSymbolicLink() : false;
    },
    IsBlock: function (path, callback) {
        return module.exports.PathExists(path) == true ? fs.lstatSync(path).isBlockDevice() : false;
    },
    /**
     * List all items in the specified directory with their informations.
     * 
     * @param {String} directory
     * @param {Boolean} showHidden
     * @param {Function} callback
     */
    ListDirectory: function (directory, showHidden, callback) {
        var content = {};
        directory = filters.Other.NormalizePath(directory);
        if (module.exports.IsDirectory(directory) === true) {
            directory = filters.Escape.Spaces(directory);
            var command = "";
            if (showHidden === false) {
                command = "ls -hl " + directory + " | tr -s ' '";
            } else {
                command = "ls -hal " + directory + " | tr -s ' '";
            }
            basis.System.ShellCommand(command, (files) => {
                if (files.stderr === "") {
                    files = files.stdout.trim().split("\n");
                    files.shift();
                    files.forEach(file => {
                        file = file.trim().split(" ");
                        var rights = file.shift();
                        var type = rights.slice(0, 1) === "d" ? "folder" : rights.slice(0, 1) === "l" ? "link" : "file";
                        var linksNumber = file.shift();
                        var owner = file.shift();
                        var group = file.shift();
                        var size = file.shift();
                        var date = file.shift() + " " + file.shift() + " " + file.shift();
                        var name = file.join(' ');
                        var extension = (type == "file") ? name.includes('.') ? name.split(".")[name.split(".").length - 1] : "" : "";
                        content[name] = {
                            name: name,
                            type: type,
                            extension: extension,
                            rights: rights,
                            linksNumber: linksNumber,
                            owner: owner,
                            group: group,
                            size: size,
                            date: date
                        };
                    });
                    callback(content);
                } else {
                    callback(false);
                }
            });
        } else {
            callback(false);
        }
    },
    /**
     * Get specified file informations
     * 
     * @param {String} path
     * @param {Function} callback
     */
    FileInformations: function (path, callback) {
        path = filters.Other.NormalizePath(path);
        if (module.exports.IsFile(path) === true) {
            basis.System.ShellCommand("ls -lh " + filters.Escape.Spaces(path) + " | tr -s ' '", (file) => {
                if (file.stderr === "") {
                    var line = file.stdout.trim().split(" ");
                    var rights = line.shift();
                    var type = rights.slice(0, 1) === "d" ? "folder" : rights.slice(0, 1) === "l" ? "link" : "file";
                    var linksNumber = line.shift();
                    var owner = line.shift();
                    var group = line.shift();
                    var size = line.shift();
                    var date = line.shift() + " " + line.shift() + " " + line.shift();
                    var name = line.join(' ');
                    var extension = (type == "file") ? name.includes('.') ? name.split(".")[name.split(".").length - 1] : "" : "";
                    callback({
                        name: name,
                        type: type,
                        extension: extension,
                        rights: rights,
                        linksNumber: linksNumber,
                        owner: owner,
                        group: group,
                        size: size,
                        date: date
                    });
                } else {
                    callback(false);
                }
            });
        } else {
            callback(false);
        }
    },
    /**
     * Get specified symbolic link informations
     * 
     * @param {String} path
     * @param {Function} callback
     */
    SymLinkInformations: function (path, callback) {
        path = filters.Other.NormalizePath(path);
        if (module.exports.IsSymLink(path) === true) {
            basis.System.ShellCommand("ls -lh " + filters.Escape.Spaces(path) + " | tr -s ' '", (file) => {
                if (file.stderr === "") {
                    var line = file.stdout.trim().split(" ");
                    var rights = line.shift();
                    var linksNumber = line.shift();
                    var owner = line.shift();
                    var group = line.shift();
                    var size = line.shift();
                    var date = line.shift() + " " + line.shift() + " " + line.shift();
                    var name = line.join(' ').split(" -> ")[0];
                    var target = line.join(' ').split(" -> ")[1];
                    callback({
                        name: name,
                        target: target,
                        rights: rights,
                        linksNumber: linksNumber,
                        owner: owner,
                        group: group,
                        size: size,
                        date: date
                    });
                } else {
                    callback(false);
                }
            });
        } else {
            callback(false);
        }
    },
    /**
     * Creates a new folder at the specified path.
     * 
     * @param {String} path
     * @param {Function} callback
     */
    NewFolder: function (path, callback) {
        if (module.exports.PathExists(path) === false) {
            fs.mkdir(path, (err) => {
                callback(!err);
            });
        } else {
            callback(false);
        }
    },
    /**
     * Creates a new file at the specified path.
     * 
     * @param {String} path
     * @param {Function} callback
     */
    NewFile: function (path, callback) {
        if (module.exports.PathExists(path) === false) {
            fs.appendFile(path, '', (err) => {
                callback(!err);
            });
        } else {
            callback(false);
        }
    },
    EmptyFile: function (path, callback) {
        if (module.exports.PathExists(path) === true) {
            try {
                fs.truncate(path, (e) => {
                    callback(true);
                });
            } catch (e) {
                callback(false);
            }
        } else {
            callback(false);
        }
    },
    /**
     * Copies the file or folder from the specified path to the destinationPath.
     * 
     * @param {String} path
     * @param {String} destinationPath
     * @param {Function} callback
     */
    Copy: function (path, destinationPath, callback) {
        if (module.exports.PathExists(path) === true) {
            if (module.exports.PathExists(destinationPath) === false) {
                var ncp = require("ncp").ncp;
                ncp.limit = 10000;
                ncp(path, destinationPath, (err) => {
                    callback(!err);
                });
            } else {
                callback(false);
            }
        } else {
            callback(false);
        }
    },
    /**
     * Moves the file or folder from the specified path to the destinationPath.
     * 
     * @param {String} path
     * @param {String} destinationPath
     * @param {Function} callback
     */
    Move: function (path, destinationPath, callback) {
        if (module.exports.PathExists(path) === true) {
            if (module.exports.PathExists(destinationPath) === false) {
                const moveFile = require('move-file');
                (async () => {
                    await moveFile(path, destinationPath);
                    callback(true);
                })();
            } else {
                callback(false);
            }
        } else {
            callback(false);
        }
    },
    /**
     * Deletes the file or folder at the specified path.
     * 
     * @param {String} path
     * @param {Function} callback
     */
    Delete: function (path, callback) {
        if (module.exports.PathExists(path) === true) {
            var rimraf = require("rimraf");
            rimraf(path, (err) => {
                callback(err === null);
            });
        } else {
            callback(false);
        }
    },
    /**
     * Reads the file at the specified path.
     * 
     * @param {String} path
     * @param {Function} callback
     */
    ReadFile: function (path, callback) {
        if (module.exports.PathExists(path) === true) {
            fs.readFile(path, 'binary', (err, data) => {
                callback(!err ? data : false);
            });
        } else {
            callback(false);
        }
    },
    /**
     * Overwrites the file's content at the specified path;
     * 
     * @param {String} path
     * @param {String} data
     * @param {Function} callback
     */
    OverwriteFileContent: function (path, data, callback) {
        if (module.exports.PathExists(path) === true) {
            fs.writeFile(path, data, 'utf-8', (err) => {
                callback(!err);
            });
        } else {
            callback(false);
        }
    },
    /**
     * Appends content to the file at the specified path;
     * 
     * @param {String} path
     * @param {String} data
     * @param {Function} callback
     */
    AppendContentToFile: function (path, data, callback) {
        if (module.exports.PathExists(path) === true) {
            fs.appendFile(path, data, 'utf-8', (err) => {
                callback(!err);
            });
        } else {
            callback(false);
        }
    }
};